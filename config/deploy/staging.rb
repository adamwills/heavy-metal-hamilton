set :stage, :production

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

set :deploy_to, -> { "~/domains/stage.heavymetalhamilton.ca/html" }

# Extended Server Syntax
# ======================
server 's184475.gridserver.com', user: 'adamwills.com', roles: %w{web app db}

SSHKit.config.command_map[:composer] = "php -d memory_limit=512M -d allow_url_fopen=1 -d suhosin.executor.include.whitelist=phar ~/composer.phar"
SSHKit.config.command_map[:npm] = "/home/184475/data/opt/bin/npm"

set :grunt_tasks, 'build'

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
#  set :ssh_options, {
#    keys: %w(~/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }

fetch(:default_env).merge!(wp_env: :production)

after 'deploy:publishing', 'gridserver:create_relative_symlinks'
