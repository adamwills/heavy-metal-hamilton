<?php
/*
Plugin Name: Heavy Metal Hamilton API
Version: 0.0.1
Plugin URI: http://heavymetalhamilton.ca
Description: An API for app integration for Heavy Metal Hamilton
Author: Adam Wills
Author URI: http://adamwills.com
Text Domain: hmh-api
License: GPL v3
*/

function hmh_api_init() {
	global $hmh_api_show;

	$hmh_api_show = new HMH_API_Shows();
	add_filter( 'json_endpoints', array( $hmh_api_show, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'hmh_api_init' );

class HMH_API_Shows {
	public function register_routes( $routes ) {
		$routes['/hmh-api/shows'] = array(
			array( array( $this, 'get_shows'), WP_JSON_Server::READABLE ),
		);
		$routes['/hmh-api/shows/(?P<id>\d+)'] = array(
			array( array( $this, 'get_show'), WP_JSON_Server::READABLE ),
		);

		// Add more custom routes here

		return $routes;
	}

	public function get_shows() {

		$today = date("Ymd",mktime(0,0,0,date("m"),date("d"),date("Y")));

		$args = array(
			'post_type' => 'show',
			'posts_per_page' => -1,
			'meta_key' => 'event_date',
			'orderby' => 'meta_value',
			'order' => 'ASC',
			'meta_query' => array(
			    array(
			      'key' => 'event_date',
			      'value' => $today,
			      'type' => 'DATE',
			      'compare' => '>',
			    )
			)
		);

		$shows_query = new WP_Query( $args );
		$shows_list = $shows_query->query( $args );
		$response = new WP_JSON_Response();
		if ( ! $shows_list ) {
			$response->set_data( array() );
			return response;
		}

		// holds all the posts data
		$struct = array();

		$response->header( 'Last-Modified', mysql2date( 'D, d M Y H:i:s', get_lastpostmodified( 'EDT' ), 0 ).' EDT' );

		foreach ( $shows_list as $post ) {
			$post = get_object_vars( $post );

			$response->link_header( 'item', json_url( '/posts/' . $post['ID'] ), array( 'title' => $post['post_title'] ) );
			unset($post['post_author'], $post['ping_status'], $post['post_password'], $post['to_ping'], $post['pinged']);
			$post_data = $post;
			if ( is_wp_error( $post_data ) ) {
				continue;
			}

			$struct[] = $post_data;
		}
		$response->set_data( $struct );

		return $response;

	}

}