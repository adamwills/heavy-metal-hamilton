<h1>Upcoming Shows</h1>

<?php 
$today = date("Ymd",mktime(0,0,0,date("m"),date("d"),date("Y")));

$args = array(
	'post_type' => 'show',
	'posts_per_page' => -1,
	'meta_key' => 'event_date',
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'meta_query' => array(
	    array(
	      'key' => 'event_date',
	      'value' => $today,
	      'type' => 'DATE',
	      'compare' => '>=',
	    )
	)
);

$the_query = new WP_Query( $args );
 ?>


<div class="show-listings">

<?php // The Loop

if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="show-listing">
			<h2><?php echo get_field('event_date'); ?><br /><?php the_title(); ?></h2>
			<div class="show-data">
				<span class="show-venue"><?php $venues = get_field('venue'); echo $venues[0]->post_title; ?></span>
				<div class="actions">
					<a class="btn btn-primary" href="<?php the_permalink(); ?>">Show details</a>
					<a href="<?php the_field('event_page'); ?>" class="btn btn-primary">Event Page</a>
					<a href="<?php the_field('ticket_link'); ?>" class="btn btn-primary">Tickets</a>
				</div>
			</div>
		</div>


	<?php endwhile; ?>

<?php } else {
	echo "No shows upcoming";
}
/* Restore original Post Data */

?>

<?php wp_reset_query(); ?>

</div>