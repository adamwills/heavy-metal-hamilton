<footer class="content-info" role="contentinfo">
  <div class="container">
    <p>Heavy Metal Hamilton is simply a site that helps the Hamilton metal community connect through promoting local shows, bands and businesses.</p>

    <div class="row">
    	<div class="col-sm-6 col-twitter">
			<img src="<?php echo get_template_directory_uri() . '/assets/img/logo-small.svg'; ?>" alt="" class="icon-twitter">
    	</div>
    	<div class="col-sm-6">
    		<h3>Contact Us</h3>
    		<p>Have some info that we're missing? Let us know!</p>
    		<p class="contact">
    			<a href="mailto:info@heavymetalhamilton.ca">info@heavymetalhamilton.ca</a> | <a href="https://twitter.com/hvymtlhamilton">@hvymtlhamilton</a>
    		</p>

		</div>
    	
    </div>
    <div class="footer-logo">
    	
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
