<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <?php $venue = get_field( 'venue' ); ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <h2><?php echo $venue[0]->post_title; ?> - <?php echo get_field('event_date'); ?></h2>
    </header>
    <div class="entry-meta">
        <a class="btn btn-primary" href="<?php the_permalink(); ?>">Show details</a>
        <a href="<?php the_field('event_page'); ?>" class="btn btn-primary">Event Page</a>
        <a href="<?php the_field('ticket_link'); ?>" class="btn btn-primary">Tickets</a>
    </div>

    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer class="disclaimer">
      <p>Note: Heavy Metal Hamilton is not the official promoter for this show. Be sure to check out the <a href="<?php the_field('event_page'); ?>">official event page</a> for the most up-to-date information on this show.</p>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
